package main

import (
	"os"

	"github.com/narqo/go-badge"
	"github.com/tdewolff/minify/v2"
	"github.com/tdewolff/minify/v2/svg"
)

func main() {
	if len(os.Args) != 4 {
		println("Usage:", os.Args[0], "subject status colour\n")
		os.Exit(1)
	}

	min := minify.New()
	min.AddFunc("image/svg+xml", svg.Minify)
	w := min.Writer("image/svg+xml", os.Stdout)

	if err := badge.Render(
		os.Args[1], os.Args[2], badge.Color(os.Args[3]), w,
	); err != nil {
		panic(err)
	}

	if err := w.Close(); err != nil {
		panic(err)
	}
}
