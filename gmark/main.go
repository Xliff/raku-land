package main

import (
	"bytes"
	"fmt"
	"io/fs"
	"os"
	"path"
	"path/filepath"

	chtml "github.com/alecthomas/chroma/formatters/html"
	"github.com/microcosm-cc/bluemonday"
	"github.com/tdewolff/minify/v2/minify"
	"github.com/yuin/goldmark"
	emoji "github.com/yuin/goldmark-emoji"
	highlighting "github.com/yuin/goldmark-highlighting"
	"github.com/yuin/goldmark/ast"
	"github.com/yuin/goldmark/extension"
	"github.com/yuin/goldmark/parser"
	"github.com/yuin/goldmark/renderer/html"
	"github.com/yuin/goldmark/util"
)

// Lifted from https://github.com/yuin/goldmark/blob/master/parser/parser.go
// with the "rl-" prefix added.
//
// TODO Turn this into an upstream PR to add a prefix instead.
type ids struct{ values map[string]bool }

func newIDs() parser.IDs { return &ids{values: map[string]bool{}} }

func (s *ids) Generate(value []byte, kind ast.NodeKind) []byte {
	value = util.TrimLeftSpace(value)
	value = util.TrimRightSpace(value)
	result := []byte("rl-")
	for i := 0; i < len(value); {
		v := value[i]
		l := util.UTF8Len(v)
		i += int(l)
		if l != 1 {
			continue
		}
		if util.IsAlphaNumeric(v) {
			if 'A' <= v && v <= 'Z' {
				v += 'a' - 'A'
			}
			result = append(result, v)
		} else if util.IsSpace(v) || v == '-' || v == '_' {
			result = append(result, '-')
		}
	}
	if len(result) == 0 {
		if kind == ast.KindHeading {
			result = []byte("heading")
		} else {
			result = []byte("id")
		}
	}
	if _, ok := s.values[util.BytesToReadOnlyString(result)]; !ok {
		s.values[util.BytesToReadOnlyString(result)] = true
		return result
	}
	for i := 1; ; i++ {
		newResult := fmt.Sprintf("%s-%d", result, i)
		if _, ok := s.values[newResult]; !ok {
			s.values[newResult] = true
			return []byte(newResult)
		}

	}
}

func (s *ids) Put(value []byte) {
	s.values[util.BytesToReadOnlyString(value)] = true
}

func main() {
	policy := bluemonday.UGCPolicy()

	// So syntax highlighting works.
	policy.AllowAttrs("class").Globally()

	markdown := goldmark.New(
		goldmark.WithExtensions(
			emoji.Emoji,
			extension.GFM,
			highlighting.NewHighlighting(
				highlighting.WithFormatOptions(chtml.WithClasses(true)),
			),
		),
		goldmark.WithParserOptions(parser.WithAutoHeadingID()),
		goldmark.WithRendererOptions(html.WithUnsafe()),
	)

	if err := filepath.WalkDir("/cache", func(file string, d fs.DirEntry, err error) error {
		if err != nil {
			return err
		}

		// Supported filenames.
		switch path.Base(file) {
		case "Changes", "Changes.md", "ChangeLog", "CHANGELOG.md":
		case "README", "README.md", "README.markdown", "readme.md":
		default:
			return nil
		}

		contents, err := os.ReadFile(file)
		if err != nil {
			return err
		}

		htmlFile := "/cache/html" + file[6:] + ".html"
		if err := os.MkdirAll(filepath.Dir(htmlFile), os.ModePerm); err != nil {
			return err
		}

		/// Plain text files are santized and wrapped a <pre>.
		if path.Ext(file) == "" {
			return os.WriteFile(
				htmlFile,
				append(
					append([]byte("<pre>"), policy.SanitizeBytes(contents)...),
					"</pre>"...,
				),
				os.ModePerm,
			)
		}

		// FIXME Chroma can panic on invalid Raku :-(
		defer func() {
			if r := recover(); r != nil {
				fmt.Printf("%s: %v\n", file, r)
			}
		}()

		// Custom "rl-" prefixed IDs.
		ids := parser.WithContext(parser.NewContext(parser.WithIDs(newIDs())))

		var buf bytes.Buffer
		if err := markdown.Convert(contents, &buf, ids); err != nil {
			return err
		}

		htmlContents, err := minify.HTML(policy.Sanitize(buf.String()))
		if err != nil {
			return err
		}

		return os.WriteFile(htmlFile, []byte(htmlContents), os.ModePerm)
	}); err != nil {
		panic(err)
	}
}
