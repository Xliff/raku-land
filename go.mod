module gitlab.com/raku-land/raku-land

go 1.21

require (
	github.com/alecthomas/chroma v0.9.2
	github.com/microcosm-cc/bluemonday v1.0.25
	github.com/narqo/go-badge v0.0.0-20230821190521-c9a75c019a59
	github.com/tdewolff/minify/v2 v2.12.9
	github.com/yuin/goldmark v1.5.6
	github.com/yuin/goldmark-emoji v1.0.2
	github.com/yuin/goldmark-highlighting v0.0.0-20210516132338-9216f9c5aa01
)

require (
	github.com/aymerick/douceur v0.2.0 // indirect
	github.com/danwakefield/fnmatch v0.0.0-20160403171240-cbb64ac3d964 // indirect
	github.com/dlclark/regexp2 v1.10.0 // indirect
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0 // indirect
	github.com/gorilla/css v1.0.0 // indirect
	github.com/tdewolff/parse/v2 v2.6.8 // indirect
	golang.org/x/image v0.12.0 // indirect
	golang.org/x/net v0.15.0 // indirect
)
