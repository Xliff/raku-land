unit module Crypt::Rand;

use NativeCall;

enum flags is export (
    GRND_NONBLOCK => 1,
    GRND_RANDOM   => 2,
);

sub __errno_location(                   ) of Pointer[int32] is native {}
sub        getrandom(Buf, size_t, uint32) of ssize_t        is native {}
sub         strerror(              int32) of Str            is native {}

multi crypt-rand(Int:D $len, Int:D $flags = 0) of Buf:D is export {
    return crypt-rand Buf.allocate($len), $flags;
}

multi crypt-rand(Buf:D $buf, Int:D $flags = 0) of Buf:D is export {
    my $len = getrandom $buf, $buf.elems, $flags;

    die 'getrandom: ', strerror __errno_location.deref if $len == -1;
    die 'getrandom: Not enough bytes'                  if $len != $buf.elems;

    return $buf;
}
