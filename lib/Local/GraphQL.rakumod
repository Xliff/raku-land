unit module Local::GraphQL;

use HTTP::Tiny;
use JSON::Fast;

my constant DEBUG = False;

sub graphql($host, $query) is export {
    my %headers = :content-type<application/json>;

    my $url = do given $host {
        when 'gitlab' { 'https://gitlab.com/api/graphql' }
        when 'github' {
            # Set GitHub auth or warn and return if not set (useful in dev).
            with %*ENV<GITHUB_TOKEN> {
                %headers<authorization> = "Bearer $_";
            }
            else {
                note 'Skipping GitHub GraphQL request, GITHUB_TOKEN not set';
                return;
            }

            'https://api.github.com/graphql';
        }
        default { die "Unknown host: $host" }
    }

    state $ua   = HTTP::Tiny.new :throw-exceptions;
    my %res     = $ua.post: $url, :content(to-json { :$query }), :%headers;
    my %content = %res<content>.decode.&from-json;

    my $complexity = %content<data><queryComplexity>:delete;
    if DEBUG {
        with %res<headers> {
            printf "$host: %4d/%4d requests left, resets at %s%s\n",
                .<ratelimit-remaining> // .<x-ratelimit-remaining>,
                .<ratelimit-limit>     // .<x-ratelimit-limit>,
                DateTime.new(+(.<ratelimit-reset> // .<x-ratelimit-reset>)),
                (sprintf ', %3d/%3d complexity', .<score>, .<limit>
                    with $complexity) // '';
        }
    }

    note "$host: $_<message>" for %content<errors>[] // [];

    return %content<data>.values.grep: *.defined;
}
