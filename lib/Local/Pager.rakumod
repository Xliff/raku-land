unit class Local::Pager;

need Cro::Uri::HTTP;

subset PosInt is export of Int where * > 0;

has PosInt $.page     = 1;
has PosInt $.per-page = 10;
has    Int $.total    = 0;
has        %!query is built;

has ($.prev, $.first, $.last, $.next);

method TWEAK {
    constant $base = Cro::Uri::HTTP.parse-relative: '';

    my $last-page = max 1, ceiling $!total / $.per-page;

    $!first = $!total ?? ( $.page - 1 ) * $.per-page + 1 !! 0;
    $!last  = $.page == $last-page ?? $.total !! $.page * $.per-page;

    $!prev = $base.add-query( |%!query, page => $.page - 1 )
        if $.page > 1;

    $!next = $base.add-query( |%!query, page => $.page + 1 )
        if $.page < $last-page;
}
