unit module Local::Routes::Index;

use Cro::HTTP::Router;
use Cro::WebApp::Template;
use Local::DB;
use Local::Pager;

get sub (PosInt :$page = 1, :$q) {
    my @authors = db.query( q:to/SQL/, $q ).hashes if $page == 1 && $q.?chars;
        SELECT avatar, id, name,
               ts_headline('english', id,   query) id_hl,
               ts_headline('english', name, query) name_hl
          FROM authors, websearch_to_tsquery('english', $1) query
         WHERE query IS NULL OR numnode(query) = 0 OR search @@ query
      ORDER BY ts_rank_cd(search, query) DESC, name
         LIMIT 9
    SQL

    my @dists = db.query( q:to/SQL/, $q, ( $page - 1 ) * 10 ).hashes;
        SELECT date, description, eco, name, slug, stars, tags,
               ts_headline('english',         description, query) description_hl,
               ts_headline('english',                name, query)        name_hl,
               ts_headline('english', array_to_json(tags), query)        tags_hl,
               COUNT(*) OVER()
          FROM distinct_dists, websearch_to_tsquery('english', $1) query
         WHERE query IS NULL OR numnode(query) = 0 OR search @@ query
      ORDER BY ts_rank_cd(search, query) DESC, name, version_order DESC
         LIMIT 10 OFFSET $2
    SQL

    my $pager = Local::Pager.new
        :$page :query(%( :$q if ?$q )) :total(@dists[0]<count> // 0);

    # Falling off the end is a 404, but no results isn't.
    return not-found if !$pager.total && $pager.page > 1;

    my %sets = db.query(q:to/SQL/).hashes.classify: *<set>;
        (SELECT 'popular' "set",
                name, slug, name title, stars::text "value"
           FROM distinct_dists
          WHERE stars IS NOT NULL
       ORDER BY stars DESC, name
          LIMIT 10)

        UNION ALL

        (WITH counts AS (
            SELECT author_id, COUNT(*) FROM distinct_dists GROUP BY author_id
        ) SELECT 'prolific' "set",
                 name, slug, name || ' (' || id || ')' title,
                 count::text "value"
            FROM counts
            JOIN authors ON author_id = id
        ORDER BY count DESC, author_id
           LIMIT 10)

        UNION ALL

        (SELECT 'recent' "set",
                name, slug, name title, version "value"
           FROM dists
       ORDER BY date DESC, name
          LIMIT 10)
    SQL

    template 'index.crotmp', { :@authors :@dists :$pager :$q :%sets };
}
